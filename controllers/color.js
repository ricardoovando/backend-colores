/**
 * @namespace routes/color
 */

const Color = require('../models/Color');
const ColorList = require('../models/ColorList');

const { ErrorHandler,handleError  } = require('../helpers/error')

function obtenerColor(req, res) {
    Color.find(req.params.id, function (err, colors) {
      if (err && err.code === 'NOT_FOUND') {
        throw new ErrorHandler(404, 'color no encontrado')
      } else if (err) { 
        throw new ErrorHandler(500, 'error al accesar data de colores')
      } else {
        res.json(colors);
      }
    })
}

function obtenerColores(req, res) {
    Color.getAll(req.query,function (err, colors) {
      if (err) {
        throw new ErrorHandler(500, 'error al accesar data de colores')
      } else {
        res.json(new ColorList(colors));
      }
    })
}

function crearColor(req, res) {
    let color = new Color(req.body);
    color.save(function (err, saved) {
      if (err) {
        throw new ErrorHandler(500, 'error al crear color')
      } else {
        res.json(saved);
      }
    })
}

exports.obtenerColores = obtenerColores
exports.obtenerColor = obtenerColor
exports.crearColor = crearColor