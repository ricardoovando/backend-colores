/**
 * @name ColorArray
 * @class
 */
const Color = require('./Color');

class ColorArray  {
  constructor(datos = {}) {
    return datos.map( x => new Color(x)); 
  }
  static getClass(){
    return this.toString().split ('(' || /s+/)[0].split (' ' || /s+/)[1];
  }    
  static getClassName(){
    return this.getClass();
  }    
}

module.exports = ColorArray