/**
 * @name Color
 * @class
 */
const OrigenDatos = require('../dao/OrigenDatos')
const origenDatos = new OrigenDatos()

class Color  {
  
  constructor(datos = {}) {
    this.id = datos.id;
    this.name = datos.name;
    this.color = datos.color;
    this.year = datos.year;
    this.pantone_value = datos.pantone_value;
  }

  /**
   * Encuentra los colores
   * @name find
   * @memberof Color
   * @function
   * @param {int} id
   * @param {function} callback
   */
  static find (id, callback) {
    origenDatos.consulta({id:id},(error, response) => {
      if(error){
        return callback(error)
      } else {
        return callback(null, new Color(response))
      } 
    })
  }

  /**
   * devuelve todos los colores
   * @name getAll
   * @memberof Color
   * @function
   * @param {function} callback
   */
  static getAll (parameters,callback) {
    origenDatos.consulta(parameters,(error, response) => {
      if(error){
        return callback(error)
      } else {
        return callback(null, response)
      }       
    })
  }

  /**
   * crea un color
   * @name save
   * @memberof Color
   * @function
   * @param {function} callback
   */
  save (callback) {
    origenDatos.inserta(this,(error, response) => {
      if(error){
        return callback(error)
      } else {
        return callback(null, new Color(response))
      }       
    })
  }
  static getClass(){
    return this.toString().split ('(' || /s+/)[0].split (' ' || /s+/)[1];
  }    
  static getClassName(){
    return this.getClass();
  }  
}

module.exports = Color