/**
 * @name ColorList
 * @class
 */
const ColorArray = require('./ColorArray');

class ColorList {
  constructor(datos = {}) {
    this.total_colors = datos.total_colors;
    this.total_pages = datos.total_pages;    
    this.page = datos.page;        
    this.colors =  new ColorArray(datos.colors)
  } 
  static getClass(){
    return this.toString().split ('(' || /s+/)[0].split (' ' || /s+/)[1];
  }    
  static getClassName(){
    return this.getClass();
  }    
}

module.exports = ColorList