const request = require('supertest')
const app = require('../server')

describe('Crear Color', () => {
  it('debe crear un nuevo color', async (done) => {
        const res = await request(app)
        .post('/api/v1/colores')
        .send({
          name: 'white',
          color: '#fff',
          name: 'white test',
          pantone_value: '1-xxxx'                
        })
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('id')       
        //console.log(res.body)
        done();         
  })
  it('debe traer todos los colores', async (done) => {
    const res = await request(app)
    .get('/api/v1/colores')
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('total_pages')       
    //console.log(res.body)
    done();         
  })  
  it('debe traer un color', async (done) => {
    const res = await request(app)
    .get('/api/v1/colores/2')
    expect(res.statusCode).toEqual(200)
    let Obj = {"id":2,"name":"fuchsia rose","color":"#C74375","year":2001,"pantone_value":"17-2031"};
    expect(res.body).toHaveProperty('id') 
    expect(res.body).toMatchObject(Obj)    
    //console.log(res.body)
    done();         
  }),
  it('debe fallar por no encontrar el codigo del color', async (done) => {
    const res = await request(app)
    .get('/api/v1/colores/2234234')
    expect(res.statusCode).toEqual(404)
    //console.log(res.body)
    done();         
  })          
})

  