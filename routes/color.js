/**
 * @namespace routes/color
 */
const color = require('../controllers/color')
const express = require('express')
const router = express.Router()

router.post('/colores', color.crearColor)
router.get('/colores', color.obtenerColores)
router.get('/colores/:id' ,color.obtenerColor)

module.exports = router