'use strict';

let express = require('express');
const { ErrorHandler,handleError  } = require('./helpers/error')

  let router = express.Router(),
  bodyParser = require('body-parser'),
  color = require('./routes/color'),  
  swaggerUi = require('swagger-ui-express'),
  swaggerDocument = require('./swagger.json'),
  mung = require('express-mung'),
  xml = require('./helpers/xml')('./models/');

const app = express();
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(mung.json((body, req, res) => {
  if (req.query.type && req.query.type === 'xml') {
    return xml.serialize(body);
  }
}));
app.use(mung.headers((req, res) => {
  if (req.query.type && req.query.type === 'xml') {
    res.set('Content-Type', 'text/xml');
  }
}));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', color);

app.use((err, req, res, next) => {
  handleError(err, res);
});

app.use(mung.headers((req, res) => {
  if (req.query.type && req.query.type === 'xml') {
    res.set('Content-Type', 'text/xml');
  }
}));
app.listen(process.env.PORT || 3000, function () {
})
module.exports = app;

