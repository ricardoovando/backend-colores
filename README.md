api de colores, la documentacion de los servicios se puede ver en la ruta

localmente al correrlo en el puerto 3000
http://localhost:3000/api-docs

o en heroku
https://enigmatic-journey-37004.herokuapp.com/api-docs

se agregaron los extras de docker, paginacion, documentacion con swagger, testing con jest, status code,endpoint para agregar colores, devolver en xml

-para levantar la api:
instalar dependencias:
npm install
ejecutar para correr la api:
npm start

-para correr con docker:
docker run -p 3000:3000 -d --name ricardo $(docker build -q .)

-para correr las pruebas unitarias en jest:
docker exec -it ricardo sh
npm test 
