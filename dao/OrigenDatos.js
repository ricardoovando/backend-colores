/**
 * @name OrigenDatos
 * @class
 */
const { ErrorHandler,handleError  } = require('../helpers/error')

const fs = require('fs'); 
const CSV = require('comma-separated-values')

class OrigenDatos {

    /**
     * Inicia un nuevo OrigenDatos
     * @name constructor
     * @memberof OrigenDatos
     * @function
     */
    constructor () {
      this.datos = [];  
      let data = fs.readFileSync('./data/colors.csv',{encoding:'utf8', flag:'r'}); 
      var csv = new CSV(data, {header:false,  header: ['id', 'name', 'year', 'color', 'pantone_value']}).parse();
      this.datos = csv;
    }
  
    /**
     * Consulta de datos
     * @name consulta
     * @memberof OrigenDatos
     * @function
     */
    consulta (parameters, callback) {
        if (parameters.id) {
            let result = this.datos.find(x => x.id == parameters.id)
            callback( ( !result ? { code: 'NOT_FOUND', message: 'no encontrado' } : null ) , ( result ? result : null) )
        } else {
            callback(null,{total_colors:this.datos.length, page: parseInt(parameters.page,10) || 1, total_pages: Math.ceil(this.datos.length/(parameters.page_size || 9)), colors:this.datos.slice(( (parameters.page || 1) - 1) * (parameters.page_size || 9), (parameters.page || 1) * (parameters.page_size || 9))})
        }
    }
   
    /**
     * Inserción de datos
     * @name inserta
     * @memberof OrigenDatos
     * @function
     */
    inserta (color, callback) {
        //agregar al array en memoria
        color.id = this.datos.length + 1
        this.datos.push(color)
        callback(null,color)
    }
  
  }
  


  module.exports = OrigenDatos