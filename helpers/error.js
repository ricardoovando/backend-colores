class ErrorHandler extends Error {
    constructor(statusCode, message) {
      super();
      this.statusCode = statusCode || 500;
      this.message = message;
    }
}
const handleError = (err, res) => {
    const { statusCode, message } = err;
    let stCode = statusCode || 500
    res.status(stCode).json({
      status: "error",
      statusCode:stCode,
      message
    });
};
module.exports = {
    ErrorHandler,
    handleError
}